#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/params/Params.h"
#include "cinder/gl/gl.h"

#include "rph/SoundPlayer.h"
#include "rph/SoundManager.h"

#define SOUND_SYNTH "synth.wav"
#define SOUND_JAZZ  "jazz.wav"
#define SOUND_8BIT  "8bit.wav"
#define SOUND_MULTICANAL "TestCanaux_2.wav" //"Test-LCR-Ls-Lr_audacty5p1.wav"

#define SEQ1A "SEQ1-A.wav"
#define SEQ1R "SEQ1-R.wav"
#define SEQ2A "SEQ2-A.wav"
#define SEQ2R "SEQ2-R.wav"
#define SEQ3A "SEQ3-A.wav"
#define SEQ3ATest "SEQ3A-Test.wav"
#define SEQ3R "SEQ3-R.wav"
#define SEQ4A "SEQ4-A.wav"
#define SEQ4R "SEQ4-R.wav"
#define SEQ5AA "SEQ5A-A.wav"
#define SEQ5BA "SEQ5B-A.wav"
#define SEQ5BR "SEQ5B-R.wav"
#define SEQ6A "SEQ6-A.wav"
#define SEQ6R "SEQ6-R.wav"
#define Seq7Butiner "Seq7-Butiner.wav"
#define SEQ7Danse "SEQ7-Danse.wav"
#define Seq7Frelons "Seq7-Frelons.wav"
#define Seq7Oiseau "Seq7-Oiseau.wav"
#define SEQOUVERTURE "SEQ-OUVERTURE.wav"
#define standbye "standbye.wav"

using namespace ci;
using namespace ci::app;
using namespace std;

class FadeSoundManagerApp : public App {
  public:
	void setup() override;
	void draw() override;
	void update() override;
    void playNextSong(string key);
	static void prepareSettings(Settings *settings);
    params::InterfaceGlRef  mParams;
	rph::SoundManager *mSoundManager;
    std::string             mCurrentSongName;
    float                   mFadeTime;
};

void FadeSoundManagerApp::prepareSettings(Settings *settings)
{
	settings->setConsoleWindowEnabled();
}

void FadeSoundManagerApp::setup()
{
    setWindowSize(240, 200);

    // Load sounds
    mSoundManager = rph::SoundManager::getInstance();
	mSoundManager->createSoundSystem(FMOD_SPEAKERMODE_5POINT1, 0);
	mSoundManager->createSoundSystem(FMOD_SPEAKERMODE_STEREO, 0);

	FMOD::System *system = mSoundManager->getSystem(0);
	mSoundManager->loadSound(SOUND_SYNTH, system, FMOD_UNIQUE);
	mSoundManager->loadSound(SOUND_JAZZ, system, FMOD_UNIQUE);
	mSoundManager->loadSound(SOUND_8BIT, system, FMOD_UNIQUE);

	FMOD_MODE modeLoop(FMOD_CREATESTREAM /*FMOD_UNIQUE*/); // FMOD_UNIQUE; //;FMOD_CREATESTREAM
	modeLoop = modeLoop | FMOD_LOOP_NORMAL;

	FMOD_MODE mode(FMOD_CREATESTREAM /*FMOD_UNIQUE*/);

	FMOD::System *system4 = mSoundManager->getSystem(0);
	mSoundManager->loadSound(SOUND_MULTICANAL, system4, mode, true);
	mSoundManager->loadSound(SEQ1A, system4, modeLoop, true);
	mSoundManager->loadSound(SEQ1R, system4, mode, true);
	mSoundManager->loadSound(SEQ2A, system4, modeLoop, true);
	mSoundManager->loadSound(SEQ2R, system4, mode, true);
	mSoundManager->loadSound(SEQ3A, system4, modeLoop, true);
	mSoundManager->loadSound(SEQ3ATest, system4, modeLoop, true);
	mSoundManager->loadSound(SEQ3R, system4, mode, true);
	mSoundManager->loadSound(SEQ4A, system4, modeLoop, true);
	mSoundManager->loadSound(SEQ4R, system4, mode, true);
	mSoundManager->loadSound(SEQ5AA, system4, modeLoop, true);
	mSoundManager->loadSound(SEQ5BA, system4, modeLoop, true);
	mSoundManager->loadSound(SEQ5BR, system4, mode, true);
	mSoundManager->loadSound(SEQ6A, system4, modeLoop, true);
	mSoundManager->loadSound(SEQ6R, system4, mode, true);
	mSoundManager->loadSound(Seq7Butiner, system4, mode, true);
	mSoundManager->loadSound(SEQ7Danse, system4, mode, true);
	mSoundManager->loadSound(Seq7Frelons, system4, mode, true);
	mSoundManager->loadSound(Seq7Oiseau, system4, mode, true);
	mSoundManager->loadSound(SEQOUVERTURE, system4, mode, true);
	mSoundManager->loadSound(standbye, system4, modeLoop, true);

    mCurrentSongName = "";
    mFadeTime = 4.0;
    
#define SEQ1A "SEQ1-A.wav"
#define SEQ1R "SEQ1-R.wav"
#define SEQ2A "SEQ2-A.wav"
#define SEQ2R "SEQ2-R.wav"
#define SEQ3A "SEQ3-A.wav"
#define SEQ3ATest "SEQ3A-Test.wav"
#define SEQ3R "SEQ3-R.wav"
#define SEQ4A "SEQ4-A.wav"
#define SEQ4R "SEQ4-R.wav"
#define SEQ5AA "SEQ5A-A.wav"
#define SEQ5BA "SEQ5B-A.wav"
#define SEQ5BR "SEQ5B-R.wav"
#define SEQ6A "SEQ6-A.wav"
#define SEQ6R "SEQ6-R.wav"
#define Seq7Butiner "Seq7-Butiner.wav"
#define SEQ7Danse "SEQ7-Danse.wav"
#define Seq7Frelons "Seq7-Frelons.wav"
#define Seq7Oiseau "Seq7-Oiseau.wav"
#define SEQOUVERTURE "SEQ-OUVERTURE.wav"
#define standbye "standbye.wav"

    // Create buttons for playback
    mParams = params::InterfaceGl::create("Loops", vec2(200, 160));
    mParams->addButton("Play Ethereal", std::bind(&FadeSoundManagerApp::playNextSong, this, SOUND_SYNTH));
    mParams->addButton("Play Jazz", std::bind(&FadeSoundManagerApp::playNextSong, this, SOUND_JAZZ));
    mParams->addButton("Play 8Bit", std::bind(&FadeSoundManagerApp::playNextSong, this, SOUND_8BIT));
	mParams->addButton("Play multicanal", std::bind(&FadeSoundManagerApp::playNextSong, this, SOUND_MULTICANAL));
	mParams->addButton("Play SEQ1A", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ1A));
	mParams->addButton("Play SEQ1R", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ1R));
	mParams->addButton("Play SEQ2A", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ2A));
	mParams->addButton("Play SEQ2R", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ2R));
	mParams->addButton("Play SEQ3A", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ3A));
	mParams->addButton("Play SEQ3ATest", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ3ATest));
	mParams->addButton("Play SEQ3R", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ3R));
	mParams->addButton("Play SEQ4A", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ4A));
	mParams->addButton("Play SEQ4R", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ4R));
	mParams->addButton("Play SEQ5AA", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ5AA));
	mParams->addButton("Play SEQ5BA", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ5BA));
	mParams->addButton("Play SEQ5BR", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ5BR));
	mParams->addButton("Play SEQ6A", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ6A));
	mParams->addButton("Play SEQ6R", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ6R));
	mParams->addButton("Play Seq7Butiner", std::bind(&FadeSoundManagerApp::playNextSong, this, Seq7Butiner));
	mParams->addButton("Play SEQ7Danse", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQ7Danse));
	mParams->addButton("Play Seq7Frelons", std::bind(&FadeSoundManagerApp::playNextSong, this, Seq7Frelons));
	mParams->addButton("Play Seq7Oiseau", std::bind(&FadeSoundManagerApp::playNextSong, this, Seq7Oiseau));
	mParams->addButton("Play SEQOUVERTURE", std::bind(&FadeSoundManagerApp::playNextSong, this, SEQOUVERTURE));
	mParams->addButton("Play standbye", std::bind(&FadeSoundManagerApp::playNextSong, this, standbye));

    mParams->addParam("Fade Time", &mFadeTime).min(0.1).max(10.0).step(0.1);
}

void FadeSoundManagerApp::playNextSong(string key)
{
    // Fade out current song
    if (mCurrentSongName != "") {
		mSoundManager->getSound(mCurrentSongName)->fadeOutAndStop(mFadeTime); //fadeOutAndPause(mFadeTime);
    }
    
    // Fade in next song
	mSoundManager->getSound(key)->fadeInAndStart(mFadeTime); //fadeInAndPlay(mFadeTime);

    // Save current song
    mCurrentSongName = key;
}

void FadeSoundManagerApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
    mParams->draw();
}

void FadeSoundManagerApp::update()
{
	mSoundManager->update();
}

CINDER_APP(FadeSoundManagerApp, RendererGl, &FadeSoundManagerApp::prepareSettings)
