#include "SoundCommon.h"

#include "cinder/log.h"
#include "cinder/app/App.h"

#include "fmod_errors.h"

using namespace ci;
using namespace ci::app;
using namespace std;

namespace rph {

	bool ERRCHECK_fn(FMOD_RESULT result)
	{
		if (result != FMOD_OK && result != FMOD_ERR_INVALID_HANDLE)
		{
			CI_LOG_W(FMOD_ErrorString(result));
			return false;
		}
		return true;
	}

	bool ERRCHECK_fn(FMOD_RESULT result, string name)
	{
		if (result != FMOD_OK && result != FMOD_ERR_INVALID_HANDLE)
		{
			CI_LOG_W("name : " + name + ", error :" + FMOD_ErrorString(result));
			return false;
		}
		return true;
	}
}