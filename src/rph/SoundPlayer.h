/*
 * SoundPlayer.h
 *
 * Created by Adria Navarro on 9/8/14.
 *
 * Copyright (c) 2014, Red Paper Heart, All rights reserved.
 * This code is intended for use with the Cinder C++ library: http://libcinder.org
 *
 * To contact Red Paper Heart, email hello@redpaperheart.com or tweet @redpaperhearts
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in
 * the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#pragma once

#include "cinder/app/App.h"
//#include "cinder/audio/SamplePlayerNode.h"
//#include "cinder/audio/GainNode.h"
//#include "cinder/audio/PanNode.h"
#include "cinder/Timeline.h"

#include "FMOD.hpp"
#include "SoundCommon.h"

using namespace ci;
//using namespace audio;

namespace rph {
    
    typedef std::shared_ptr<class SoundPlayer> SoundPlayerRef;
    
	//#define ERRCHECK(_result) ERRCHECK_fn(_result)
	//void ERRCHECK_fn(FMOD_RESULT result);

    class SoundPlayer {
    public:
		static SoundPlayerRef create(const DataSourceRef &source, FMOD::System * system, FMOD_MODE mode = FMOD_DEFAULT, bool bStream = true);
        
		SoundPlayer();
		SoundPlayer(const DataSourceRef &source, FMOD::System * system, FMOD_MODE mode = FMOD_DEFAULT, bool bStream = true);
        ~SoundPlayer() {
			if (!mSound)
				return;
			ERRCHECK(mSound->release());
		};

        // playback
		void play()                                             { /*bool isPlaying;  ERRCHECK(mChannel->isPlaying(&isPlaying)); */ if (!mbStarted /*|| !isPlaying*/) start();  if (!mChannel) return; ERRCHECK_(mChannel->setPaused(false), "setPaused -> " + mName); /*mPlayer->enable();*/ }      // plays from beginning
		void start()											{ 
																	if (!mSystem) return; 
																	//if (mbStream && mbStarted) stop();
																	if (mbStarted && mbStream)
																	{
																		goToTime(0.0);
																		play();
																	}
																	else
																	{
																		ERRCHECK_(mSystem->playSound(mSound, 0, 0, &mChannel), "playSound -> " + mName);
																		mbStarted = true; /*mPlayer->start();*/
																	}
																}       // plays from wherever it was
		void stop()												{ 
																	if (!mChannel) return; 
																	if (mbStream)
																	{
																		pause();
																	}
																	else
																	{
																		ERRCHECK_(mChannel->stop(), "stop -> " + mName);
																	}
																	//mChannel = nullptr; 
																}
		void pause()                                            { if (!mChannel) return; ERRCHECK_(mChannel->setPaused(true), "setPaused -> " + mName); /*mPlayer->disable();*/ }     // TODO: stop timers and deal with ramps
		void setLoop()											{ mbLoop = true;  if (!mSound) return; ERRCHECK_(mSound->setMode(FMOD_LOOP_NORMAL), "setMode -> " + mName); /*mPlayer->setLoopEnabled(loop);*/ }
		//void goToFrame(size_t frame)                            { mPlayer->seek(frame);}
		void goToTime(double seconds)                           { if (!mChannel) return; ERRCHECK_(mChannel->setPosition(static_cast<unsigned int>(seconds * 1000.0), FMOD_TIMEUNIT_MS), "setPosition -> " + mName);  /*mPlayer->seekToTime(seconds);*/ }
		void goToPercent(double percent)                        { if (!mChannel) return; ERRCHECK_(mChannel->setPosition(static_cast<unsigned int>(percent * getDuration()), FMOD_TIMEUNIT_MS), "setPosition -> " + mName); /*mPlayer->seekToTime(percent * getDuration());*/ }   // 0.0 - 1.0

        // volume
		void setVolume(float volume)                            { if (!isPlaying()) return; ERRCHECK_(mChannel->setVolume(volume), "setVolume -> " + mName); /*mGain->setValue(volume);*/ }
		void fadeTo(float volume, float seconds, float delay = 0.0f)                { if (!mChannel) return; fade(getVolume(), volume, seconds, delay); /*fade(mGain->getValue(), volume, seconds);*/ }
        void fadeOutAndStop(float seconds, float delay = 0.0f);
        void fadeOutAndPause(float seconds, float delay = 0.0f);
        void fadeInAndPlay(float seconds, float volume = 1.0f, float delay = 0.0f);
        void fadeInAndStart(float seconds, float volume = 1.0f, float delay = 0.0f);

        void fade(float from, float to, float seconds, float delay = 0.0f);
        
        // pan
        //void setPan(float pos)                                  { mPan->setPos(pos); }
        //void panTo(float pos, float seconds)                    { pan(mPan->getPos(), pos, seconds); }
        //void pan(float from, float to, float seconds);
        
        // getters
		bool isPlaying() const									{ if (!mSystem) return false; if (!mSound) return false; if (!mChannel) return false; if (!mbIsLoaded) return false; bool isplaying; if(!ERRCHECK_(mChannel->isPlaying(&isplaying), "isPlaying -> " + mName)) return false; return isplaying; /*return mPlayer->isEnabled() && !isFinished();*/ }
		bool isLooping() const									{ if (!mSound) return false;  FMOD_MODE mode = 0; ERRCHECK_(mSound->getMode(&mode), "getMode -> " + mName); return mode & FMOD_LOOP_NORMAL; /*return mPlayer->isLoopEnabled();*/ }
		//bool isFinished() const                                 { return mChannel->isEof(); }
		float getVolume() const									{ if (!isPlaying()) return 0.f; float volume = 0.f;  if(!ERRCHECK_(mChannel->getVolume(&volume), "getVolume -> " + mName)) return 0.f; return volume; /*return mGain->getValue();*/ }
        //float getPan() const                                    { return mPan->getPos(); }
		float getDuration() const                               { if (!mSound) return 0.f; unsigned int length = 0; ERRCHECK_(mSound->getLength(&length, FMOD_TIMEUNIT_MS), "getLength -> " + mName); return static_cast<float>(length); }
        //size_t getDurationFrames() const                        { return mPlayer->getNumFrames(); }
        
        //SamplePlayerNodeRef getPlayerNode()                     { return mPlayer; }
        
    private:
        //SamplePlayerNodeRef mPlayer;
  //      GainNodeRef			mGain;
		//Pan2dNodeRef		mPan;

		FMOD::System		*mSystem;
		FMOD::Sound    		*mSound;
		FMOD::Channel		*mChannel;
		int					mRate;
		unsigned long long  mDspclock;

        Anim<float>         mTimer;
		bool mbStarted;
		bool mbIsLoaded;
		bool mbLoop;
		bool mbStream;
		std::string mName;
    };
}

