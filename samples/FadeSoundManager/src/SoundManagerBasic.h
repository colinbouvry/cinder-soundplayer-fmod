
#include "rph/SoundManager.h"
#include "rph/SoundSystem.h"

namespace rph {
	class SoundManagerBasic
		: public SoundManager
	{
	public:
		
		SoundManagerBasic(){};                                                    // Private so that it can  not be called
		SoundManagerBasic(SoundManagerBasic const&){};
		void setup();
		FMOD::System * getSystem(int num);
	protected:

		FMOD_RESULT       mResult;

		std::map<int, rph::SoundSystemRef> mMapSoundSystem;
	};
}