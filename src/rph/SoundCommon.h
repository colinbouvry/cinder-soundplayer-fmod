#pragma once

#include "FMOD.hpp"
#include <string>

namespace rph {
	#define ERRCHECK(_result) ERRCHECK_fn(_result)
	#define ERRCHECK_(_result, _name) ERRCHECK_fn(_result, _name)

	bool ERRCHECK_fn(FMOD_RESULT result);
	bool ERRCHECK_fn(FMOD_RESULT result, std::string name);
}