#pragma once

#include "cinder/app/App.h"

#include "FMOD.hpp"

namespace rph {
	typedef std::shared_ptr<class SoundSystem> SoundSystemRef;
	class SoundSystem
	{
	public:
		SoundSystem();
		~SoundSystem();
		static SoundSystemRef create(){ return std::make_shared<SoundSystem>(); }
		void setup(int numDriver, FMOD_SPEAKERMODE speakerMode, int samplerate = 48000);
		FMOD::System	* getSystem() { return mSystem; }
		void update();
	protected:
		FMOD_RESULT fetchDriver(FMOD::System *system);
		FMOD::System	*mSystem;
		int               mDriver;
		FMOD_SPEAKERMODE  mSpeakermode;
		FMOD_RESULT       mResult;
	};
}