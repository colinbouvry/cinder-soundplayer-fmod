/*
 * SoundPlayer.cpp
 *
 * Created by Adria Navarro on 9/8/14.
 *
 * Copyright (c) 2014, Red Paper Heart, All rights reserved.
 * This code is intended for use with the Cinder C++ library: http://libcinder.org
 *
 * To contact Red Paper Heart, email hello@redpaperheart.com or tweet @redpaperhearts
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in
 * the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "SoundPlayer.h"
#include "SoundCommon.h"

#include "cinder/log.h"

#include "fmod_errors.h"

using namespace ci;
using namespace ci::app;
using namespace std;

namespace rph {
    
	SoundPlayerRef SoundPlayer::create(const DataSourceRef &source, FMOD::System * system, FMOD_MODE mode, bool bStream)
    {
		SoundPlayerRef ref(new SoundPlayer(source, system, mode, bStream));
        return ref;
    }

	SoundPlayer::SoundPlayer()
		: mbStarted(false)
		, mSystem(nullptr)
		, mSound(nullptr)
		, mChannel(nullptr)
		, mbIsLoaded(false)
		, mbLoop(false)
		, mbStream(false)
	{
	}
	SoundPlayer::SoundPlayer(const DataSourceRef &source, FMOD::System * system, FMOD_MODE mode, bool bStream)
		: mbStarted(false)
		, mSystem(nullptr)
		, mSound(nullptr)
		, mChannel(nullptr)
		, mbIsLoaded(false)
		, mbLoop(false)
		, mbStream(false)
    {
		mSystem = system;
		mName = source->getFilePath().string();
		mbStream = bStream;
		if (bStream) 
			mbIsLoaded = ERRCHECK_(mSystem->createStream(source->getFilePath().string().c_str(), mode, NULL, &mSound), "createStream -> " + mName);
		else
			mbIsLoaded = ERRCHECK(mSystem->createSound(source->getFilePath().string().c_str(), mode, NULL, &mSound), "createSound -> " + mName);

		if (mbIsLoaded)
			ERRCHECK_(mSystem->getSoftwareFormat(&mRate, 0, 0), "getSoftwareFormat -> " + mName);                // Get mixer rate

		mbLoop = mode & FMOD_LOOP_NORMAL;

		mTimer = Anim<float>(0.f);
    }
    
    void SoundPlayer::fadeInAndPlay(float seconds, float volume, float delay)
    {
		if (!mbIsLoaded) return;

		mTimer.stop();
		if (delay <= 0.f)
			play();
		else
		{
			cinder::app::timeline().apply(&mTimer, 0.0f, delay).finishFn(std::bind(&SoundPlayer::play, this));
		}

		try
		{
			fade(0.0f, volume, seconds, delay);
		}
		catch (const std::exception & ex)
		{
			CI_LOG_EXCEPTION("fadeTo ", ex);
		}
    }
    
    void SoundPlayer::fadeInAndStart(float seconds, float volume, float delay)
    {
		if (!mbIsLoaded) return;

		mTimer.stop();
		if (delay <= 0.f)
			start();
		else
		{
			cinder::app::timeline().apply(&mTimer, 0.0f, delay).finishFn(std::bind(&SoundPlayer::start, this));
		}

		try
		{
			fade(0.0f, volume, seconds, delay);
		}
		catch (const std::exception & ex)
		{
			CI_LOG_EXCEPTION("fadeTo ", ex);
		}
    }

    void SoundPlayer::fadeOutAndStop(float seconds, float delay)
    {
		if (!mbIsLoaded) return;

        // There's no thread safe callbacks from audio ramps yet, so we just hack it with timeline
		mTimer.stop();
        cinder::app::timeline().apply(&mTimer, 0.0f, seconds + delay).finishFn(std::bind(&SoundPlayer::stop, this));

		try {
			fadeTo(0.0f, seconds, delay);
		}
		catch (const std::exception & ex)
		{
			CI_LOG_EXCEPTION("fadeTo ", ex);
		}
    }

	void SoundPlayer::fadeOutAndPause(float seconds, float delay)
	{
		if (!mbIsLoaded) return;

		// There's no thread safe callbacks from audio ramps yet, so we just hack it with timeline
		mTimer.stop();
		cinder::app::timeline().apply(&mTimer, 0.0f, seconds + delay).finishFn(std::bind(&SoundPlayer::pause, this));

		try {
			fadeTo(0.0f, seconds, delay);
		}
		catch (const std::exception & ex)
		{
			CI_LOG_EXCEPTION("fadeTo ", ex);
		}
	}
    
	// http://www.fmod.org/questions/question/how-do-i-fade-in-and-fade-out-a-channel-with-stop/
    void SoundPlayer::fade(float from, float to, float seconds, float delay)
    {
		if (!mbIsLoaded) return;
		if (!mChannel) return;
		if (!isPlaying()) return;
        /*mGain->getParam()->applyRamp(from, to, seconds, Param::Options().delay(delay));*/
		float delayRate = (mRate * delay);
		ERRCHECK_(mChannel->getDSPClock(0, &mDspclock), "getDSPClock -> " + mName);
		// Get the reference clock, which is the parent channel group
		ERRCHECK_(mChannel->addFadePoint(mDspclock + delayRate, from), "addFadePoint -> " + mName);					// Add a fade point at 'now' with full volume.
		ERRCHECK_(mChannel->addFadePoint(mDspclock + delayRate + (mRate * seconds), to), "addFadePoint -> " + mName);  // Add a fade point 5 seconds later at 0 volume.
    }
    
    //void SoundPlayer::pan(float from, float to, float seconds)
    //{
    //    // no getParam in Pan node?
    //    // mPan->getParam()->applyRamp(from, pos, seconds);
    //}
}