#include "SoundManagerBasic.h"

#include "cinder/log.h"

using namespace ci;
using namespace ci::app;
using namespace std;

namespace rph {
	void SoundManagerBasic::setup()
	{
		rph::SoundSystemRef soundSystem = rph::SoundSystem::create();
		soundSystem->setup(4, FMOD_SPEAKERMODE_SURROUND /*FMOD_SPEAKERMODE_5POINT1*/);
		mMapSoundSystem[4] = soundSystem;

		rph::SoundSystemRef soundSystem2 = rph::SoundSystem::create();
		soundSystem2->setup(0, FMOD_SPEAKERMODE_STEREO);
		mMapSoundSystem[0] = soundSystem2;
	}

	FMOD::System * SoundManagerBasic::getSystem(int num)
	{
		auto iter = mMapSoundSystem.find(num);
		if (iter != mMapSoundSystem.end()) 
			return iter->second->getSystem();
		return nullptr;
	}
}