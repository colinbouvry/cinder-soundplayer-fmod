#include "SoundSystem.h"

#include "cinder/log.h"

#include "SoundCommon.h"

using namespace ci;
using namespace ci::app;
using namespace std;

namespace rph {

	SoundSystem::SoundSystem()
		: mSystem(nullptr)
	{

	}

	SoundSystem::~SoundSystem()
	{
		if (!mSystem)
			return;

		ERRCHECK(mSystem->close());
		ERRCHECK(mSystem->release());
	}

	void SoundSystem::setup(int numDriver, FMOD_SPEAKERMODE speakerMode, int samplerate)
	{
		mResult = FMOD::System_Create(&mSystem);
		ERRCHECK(mResult);

		mResult = fetchDriver(mSystem);
		ERRCHECK(mResult);

		mDriver = numDriver;
		CI_LOG_I("Select driver " << mDriver);

		mResult = mSystem->setDriver(mDriver);
		ERRCHECK(mResult);

		mResult = mSystem->setSoftwareFormat(samplerate, speakerMode, 0);
		ERRCHECK(mResult);

		//mResult = mSystem->setStreamBufferSize(16384 * 4, FMOD_TIMEUNIT_RAWBYTES);
		//ERRCHECK(mResult);

		//mResult = mSystem->setDSPBufferSize(1024, 16);
		//ERRCHECK(mResult);

		mResult = mSystem->init(32, FMOD_INIT_NORMAL, NULL); // mSystem->init( 32, FMOD_INIT_NORMAL | FMOD_INIT_ENABLE_PROFILE, NULL );
		ERRCHECK(mResult);

		mResult = mSystem->getSoftwareFormat(0, &mSpeakermode, 0);
		ERRCHECK(mResult);
	}

	void SoundSystem::update()
	{
		if (!mSystem)
			return;
		ERRCHECK(mSystem->update());
	}

	FMOD_RESULT SoundSystem::fetchDriver(FMOD::System *system)
	{
		FMOD_RESULT result;
		int numdrivers;

		result = system->getNumDrivers(&numdrivers);
		ERRCHECK(result);

		if (numdrivers == 0)
		{
			result = system->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
			ERRCHECK(result);
		}

		ostringstream oss;
		oss << "==================================================" << endl;
		oss << "Multiple System Example." << endl;
		oss << "Copyright (c) Firelight Technologies 2004-2016." << endl;
		oss << "==================================================" << endl;
		oss << "" << endl;
		oss << "Choose a device for system: " << system << endl;
		oss << "" << endl;
		oss << "" << endl;
		for (int i = 0; i < numdrivers; i++)
		{
			char name[256];

			result = system->getDriverInfo(i, name, sizeof(name), 0, 0, 0, 0);
			ERRCHECK(result);
			oss << "Index " << i << " " << name << endl;
			/*console() << "[%c] - %d. %s", selectedindex == i ? 'X' : ' ', i, name << endl;*/
		}
		CI_LOG_I(oss.str());

		return FMOD_OK;
	}
}